/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */
exports.createSSE = (req, res) => {
    let message = req.query.message || req.body.message || 'Hello World!';

    // SSE Setup
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
    });

    let messageId = 0;

    const intervalId = setInterval(() => {
        var myData = JSON.stringify({ 
            id: messageId, 
            data: "Test Message -- " + Date.now()
        });
        res.write(myData + "\n");

        messageId += 1;
    }, 1000);

    req.on('close', () => {
        clearInterval(intervalId);
    });
  };
  