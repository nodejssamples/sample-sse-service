const express = require('express');

const app = express();

function sseDemo(req, res) {
    let messageId = 0;

    const intervalId = setInterval(() => {
        var myData = JSON.stringify({ 
            id: messageId, 
            data: "Test Message -- " + Date.now()
        });
        res.write(myData + "\n");

        messageId += 1;
    }, 1000);

    req.on('close', () => {
        clearInterval(intervalId);
    });
}

app.get('/', (req, res) => {
    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    console.log(fullUrl);

    // SSE Setup
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
    });

    sseDemo(req, res);
});

const port = process.env.PORT || 80;
app.listen(port, () => {
  console.log('SSE is up & running on port: ', port);
});